# Merge/Split PDF

Small WPF application that let you:
- Merge multiple PDF files into one file
- Split a file into multiple files
- Interleave multiple files. Can be usefull when scanning double sided prints:
	- Scan the odd pages in batch (into one PDF-document)
	- Scan the even pages in batch (into one PDF-document)
	- Interleave the two documents

Per file you can specify which pages to use.